// https://maps.googleapis.com/maps/api/geocode/json?address=Berlin+62,+barcelona&key=AIzaSyAAC6ZjRn-OHTI0jAm_DMcshINrq4pJY6w

var json = require('./coords-sigfox-clients-data/minicustomers.json');
var request = require('request');
var async = require("async");
var fs = require('fs');
const utf8 = require('utf8');


let link_base = "https://maps.googleapis.com/maps/api/geocode/json?address=";
let API_KEY = "&key=AIzaSyAAC6ZjRn-OHTI0jAm_DMcshINrq4pJY6w";
var coords = [];
var acum = 0;

async.forEachOfSeries(json.Sheet1, function (value, key, callback) { //tiene que ser sercuencial y no paralelo pq la Api falla con muchas llamadas en paralelo
  let link = link_base + value[7] + ",+" + value[6] + API_KEY;
  var link2 = link.replace(/\s/g, '+');
  request(utf8.encode(link2), function (error, response, body) {
    if (!error) {
      try {
        var info = JSON.parse(body);
        if (info.results[0] === undefined) {
          console.log("key " + key + " is undefined :(");
          console.log("body: " + body);
          coords[key] = "null";
        }
        else {
          coords[key] = info.results[0].geometry.location.lat + "," + info.results[0].geometry.location.lng;
          console.log("Elemento actual: " + key);
        }
      } catch (e) {
        console.log("ERROR on key " + key);
        console.log("Link: " + link2);
        console.log("body: " + body);
        console.log("************");
      }
    }
    return callback();
  });
}, function (err) {
    if (err) console.error(err.message);
    var file = fs.createWriteStream('coords-africa-data/sample.txt');
    file.on('error', function(err) { /* error handling */ });
    coords.forEach(function(v) { file.write(v + '\n'); });
    file.end();
    console.log("Execution ended");
});



/*[
  0 - "Customer Name",
  1 - "Customer Alias",
  2 - "Party Classification Group",
  3 - "Customer Classification Group",
  4 - "Country Code",
  5 - "Postal Code",
  6 - "City",
  7 - "Street"
]*/
