
function job(params, callback) {
    thethingsAPI.getProductThings((err, things) => {
        let warehouseAssociation = {};
        let warehouseAssociationList = {};
        let stores = things.filter(thing => {
            let storeTag = thing.tags.filter(tag => (tag._id == 'car') || (tag._id == 'cac') || (tag._id == 'shop') || (tag._id == 'cityshop'));
            return storeTag.length > 0;
        }).map(store => {
            let geo = {};
            warehouseAssociationList[store.description.name] = '';
            warehouseAssociation[store.description.name] = 0;
            geo[store.description.name] = {
                latitude: store.description.geo.lat,
                longitude: store.description.geo.long
            };
            return geo;
        });


        let boxes = things.filter(thing => thing.tags.filter(tag => tag._id == 'cage').length > 0);
        async.eachSeries(boxes, function (thing, next) {
            let result = [];
            getAndWriteHorasOffline(thing)
                .then(thing => {
                    if (thing.description.hasOwnProperty('geo') && thing['horasOffline'] < 48)
                    {
                        getNearestStore(thing)
                            .then(thing => getAuxNearestStore(thing))
                            .then(thing => getOriginDestination(thing))
                            .then(thing => {
                                let geo = thing.description.geo;
                                let distancesToStores = geolib.orderByDistance({
                                        latitude: geo.lat,
                                        longitude: geo.long
                                    },
                                    stores.reduce((a, b) => Object.assign(a, b), {}));

                                let totalDistance = distancesToStores.reduce(
                                    (previousValue, currentValue) => previousValue + parseInt(currentValue.distance), 0);
                                let nearestStore = distancesToStores[0];
                                let transportStatus = 'UNKNOWN'; // Tal y como estan los if creo que no es necesario UNKNOWN
                                result.push({
                                    key: 'auxNearestStore',
                                    value: nearestStore.key
                                });

                                result.push({
                                    key: 'auxNearestStoreAccuracy',
                                    value: (1.0 - parseFloat(nearestStore.distance) / totalDistance).toFixed(2)
                                });
                                if((nearestStore.key !== thing['oldNearestStore']) && (nearestStore.key === thing['auxNearestStore'])){
                                  console.log('moving thing');
                                    result.push({
                                        key: 'nearestStore',
                                        value: nearestStore.key
                                    });
                                    result.push({
                                        key: 'nearestStoreAccuracy',
                                        value: (1.0 - parseFloat(nearestStore.distance) / totalDistance).toFixed(2)
                                    });
                                    if (nearestStore.key === thing['destination']) transportStatus = 'DESTINATION';
                                    else transportStatus = 'TRANSIT';
                                }
                                else if (nearestStore.key === thing['destination']) transportStatus = 'DESTINATION';
                                else transportStatus = 'DIFFERENT';
                                if (nearestStore.key === thing['origin']) transportStatus = 'ORIGIN';

                                if (!warehouseAssociation.hasOwnProperty(nearestStore.key)) {
                                    warehouseAssociation[nearestStore.key] = 1;
                                } else {
                                    warehouseAssociation[nearestStore.key] += 1;
                                }
                                warehouseAssociationList[nearestStore.key] += '\n' + thing.description.name;

                                result.push({
                                  key: 'transportStatus',
                                  value: transportStatus
                                });

                                thethingsAPI.thingWrite(thing.thingToken, {
                                    values: result
                                }, {"lib":"panel"}, function (err, result) {
                                    if (err) return next(null, err);
                                    return next();
                                });
                            })
                    }
                    else
                    {
                        result.push({
                                    key: 'transportStatus',
                                    value: 'OFFLINE'
                                });
                        thethingsAPI.thingWrite(thing.thingToken, {
                                    values: result
                                }, {lib:"panel"}, function (err, result) {
                                    if (err) return next(null, err);
                                    return next();
                                });
                        //return next();
                    }
                })
        }, function (err) {
            async.eachLimit(Object.keys(warehouseAssociation), 10, (warehouse, next) => {
                let warehousePayload = [{
                    key: 'associatedCages',
                    value: warehouseAssociation[warehouse]
                }];

                warehousePayload.push({
                    key: 'associatedCagesList',
                    value: warehouseAssociationList[warehouse]
                });

                let warehouseThingToken = things.filter(thing => thing.description.name === warehouse)[0].thingToken;

                thethingsAPI.thingWrite(warehouseThingToken, {
                    values: warehousePayload
                }, function (errorWriteWarehouse) {
                    if (errorWriteWarehouse) return next(null, err);
                    return next();
                });

            }, (warehouseError, warehouseResult) => {
                if (err) return callback('Error on some devices' + warehouseResult);
                return callback(null, 'Done');
            });
        });
    });
}

function getAndWriteHorasOffline(thing) {
    return new Promise(resolve => {
        thethingsAPI.thingRead(thing.thingToken, 'sgfx-payload', function (err, result) {
            if ((err)||(!result)||(result.length == 0)) thing['horasOffline'] = 0;
            else thing['horasOffline'] = moment().diff(moment(result[0].datetime), 'hours');
            var data = [{
                key : 'horasOffline',
                value : thing['horasOffline']
            }]
            thethingsAPI.thingWrite(thing.thingToken, {values:data}, {lib:'panel'}, function(err,result) {
                return resolve(thing);
            });
        });
    });
}

function getNearestStore(thing) {
    return new Promise(resolve => {
        thethingsAPI.thingRead(thing.thingToken, 'nearestStore', function (err, result) {
            if ((err)||(!result)||(result.length == 0)) thing['oldNearestStore'] = "";
            else thing['oldNearestStore'] = result[0].value;
            return resolve(thing);
        });
    });
}


function getOriginDestination(thing) {
    return new Promise(resolve => {
        thethingsAPI.thingRead(thing.thingToken, '$settings', function (err, result) {
            if ((err)||(!result)||(result.length == 0))
            {
              thing['origin'] = "CAR BARCELONA";//!!!!!
              thing['destination'] = "";

            }
            else {
                var settings = result[0].value;
                if (settings.origin) thing['origin'] = settings.origin;
                else thing['origin'] = "";
                if (settings.destination) thing['destination'] = settings.destination;
                else thing['destination'] = "";
            }
            return resolve(thing);
        });
    });
}

function getAuxNearestStore(thing) {
    return new Promise(resolve => {
        thethingsAPI.thingRead(thing.thingToken, 'auxNearestStore', function (err, result) {
            if ((err)||(!result)||(result.length == 0)) thing['auxNearestStore'] = thing['oldNearestStore'];
            else thing['auxNearestStore'] = result[0].value;
            return resolve(thing);
        });
    });
}
