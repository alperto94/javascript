
//Job de SENSATAG
function job(params, callback) {
    thethingsAPI.getProductThings(function(err, things) {
        let boxes = things.filter(thing => thing.tags.filter(tag => tag._id == 'cage').length > 0);
        async.eachSeries(boxes, function(thing, next) {
            async.waterfall([
                async.apply(checkHeatMapProperty, thing),
                getLastHourStatus,
                updateHeatMap
            ], function(err, results) {
                return next();
            });
        }, function(err) {
            if (err) return callback(err);
            callback(null, 'Done.');
        })
    });

    function checkHeatMapProperty(thing, callback) {
        let initialArray = [[0, 0, 0],[0, 1, 0],[0, 2, 0],[0, 3, 0],[0, 4, 0],[0, 5, 0],[0, 6, 0],[1, 0, 0],[1, 1, 0],[1, 2, 0],[1, 3, 0],[1, 4, 0],[1, 5, 0],[1, 6, 0],[2, 0, 0],[2, 1, 0],[2, 2, 0],[2, 3, 0],[2, 4, 0],[2, 5, 0],[2, 6, 0],[3, 0, 0],[3, 1, 0],[3, 2, 0],[3, 3, 0],[3, 4, 0],[3, 5, 0],[3, 6, 0],[4, 0, 0],[4, 1, 0],[4, 2, 0],[4, 3, 0],[4, 4, 0],[4, 5, 0],[4, 6, 0],[5, 0, 0],[5, 1, 0],[5, 2, 0],[5, 3, 0],[5, 4, 0],[5, 5, 0],[5, 6, 0],[6, 0, 0],[6, 1, 0],[6, 2, 0],[6, 3, 0],[6, 4, 0],[6, 5, 0],[6, 6, 0],[7, 0, 0],[7, 1, 0],[7, 2, 0],[7, 3, 0],[7, 4, 0],[7, 5, 0],[7, 6, 0],[8, 0, 0],[8, 1, 0],[8, 2, 0],[8, 3, 0],[8, 4, 0],[8, 5, 0],[8, 6, 0],[9, 0, 0],[9, 1, 0],[9, 2, 0],[9, 3, 0],[9, 4, 0],[9, 5, 0],[9, 6, 0],[10, 0, 0],[10, 1, 0],[10, 2, 0],[10, 3, 0],[10, 4, 0],[10, 5, 0],[10, 6, 0],[11, 0, 0],[11, 1, 0],[11, 2, 0],[11, 3, 0],[11, 4, 0],[11, 5, 0],[11, 6, 0],[12, 0, 0],[12, 1, 0],[12, 2, 0],[12, 3, 0],[12, 4, 0],[12, 5, 0],[12, 6, 0],[13, 0, 0],[13, 1, 0],[13, 2, 0],[13, 3, 0],[13, 4, 0],[13, 5, 0],[13, 6, 0],[14, 0, 0],[14, 1, 0],[14, 2, 0],[14, 3, 0],[14, 4, 0],[14, 5, 0],[14, 6, 0],[15, 0, 0],[15, 1, 0],[15, 2, 0],[15, 3, 0],[15, 4, 0],[15, 5, 0],[15, 6, 0],[16, 0, 0],[16, 1, 0],[16, 2, 0],[16, 3, 0],[16, 4, 0],[16, 5, 0],[16, 6, 0],[17, 0, 0],[17, 1, 0],[17, 2, 0],[17, 3, 0],[17, 4, 0],[17, 5, 0],[17, 6, 0],[18, 0, 0],[18, 1, 0],[18, 2, 0],[18, 3, 0],[18, 4, 0],[18, 5, 0],[18, 6, 0],[19, 0, 0],[19, 1, 0],[19, 2, 0],[19, 3, 0],[19, 4, 0],[19, 5, 0],[19, 6, 0],[20, 0, 0],[20, 1, 0],[20, 2, 0],[20, 3, 0],[20, 4, 0],[20, 5, 0],[20, 6, 0],[21, 0, 0],[21, 1, 0],[21, 2, 0],[21, 3, 0],[21, 4, 0],[21, 5, 0],[21, 6, 0],[22, 0, 0],[22, 1, 0],[22, 2, 0],[22, 3, 0],[22, 4, 0],[22, 5, 0],[22, 6, 0],[23, 0, 0],[23, 1, 0],[23, 2, 0],[23, 3, 0],[23, 4, 0],[23, 5, 0],[23, 6, 0]];
        if (!thing.description.heatMap || !Array.isArray(thing.description.heatMap) || thing.description.heatMap.length !== 168) {
            thing.description.heatMap = initialArray;
        }

        return callback(null, thing);
    }

    function getLastHourStatus(thing, callback){

        let options = {
            "lib": "panel",
            "startDate": moment().startOf('hour').subtract(1, 'hours').toJSON(),
            "endDate": moment().endOf('hour').subtract(1, 'hours').toJSON()
        };

        thethingsAPI.thingRead(thing.thingToken, 'transportStatus', options, function(err, result) {
            if (err) return callback(err);
            if (!result || result.length === 0) return callback('no data');
            callback(null, thing, result[0].value);
        });
    }

    function updateHeatMap(thing, transportStatus, callback) {
        let hour = moment().subtract(1, 'hours').hour();
        let day = (moment().subtract(1, 'hours').isoWeekday()) - 1;
        let position = hour * 7 + day;
        console.log(moment().hour());

        thing.description.heatMap[position] = [hour, day, transportStatusToNumber(transportStatus)];
        console.log("HeatMap THING:", thing.description.name);

        var params = {'lib':'panel'};
        var values = {'values':[{'key':'$settings.heatMap','value': thing.description.heatMap}]};
        thethingsAPI.thingWrite(thing.thingToken, values, params, function(err, result){
          if (err) return callback(err);
          if (result.status === 'error') return callback(err);
          callback(null, result);
        });
    }

    function transportStatusToNumber (input) {
      else if (input === "OFFLINE") return 0;
      else if (input === "ORIGIN") return 10;
      else if (input === "TRANSIT") return 20;
      else if (input === "DIFFERENT") return 30;
      else if (input === "DESTINATION") return 40;
      else {
        console.log("Error en transportStatusToNumber");
        return 50;//Algun tipo de error???
      }
    }
}
