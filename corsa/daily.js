function job(params, callback){
  thethingsAPI.getProductThings(function(err, things) {
    if (err) return callback(err);
    var productionThings = things.filter(thing => {
        var productTag = thing.tags.filter(tag => (tag._id == 'prod'));
        return productTag.length > 0;
    });

    if (productionThings == undefined || productionThings.length == 0) return callback(null, "finished");

    async.eachSeries(productionThings, function(thing, next) {
      async.waterfall([
        async.apply(getYesterdayConsumoPermeadoTotal, thing),
        getDayDifferenceConsumoPermeado,
        saveConsumoDiario
      ], function(err, result) {
        //if (err) return next(err);
        next();
      });
    }, function(err){
      callback(null, 'done');
    });
  });
}

function getYesterdayConsumoPermeadoTotal(thing, callback) {
  var yesterdayEnd = moment().endOf('day').add(-1, 'days');
  var params = {endDate: yesterdayEnd.toJSON(), lib:'panel'};
  thethingsAPI.thingRead(thing.thingToken, 'Consumo_Permeado', params, function(err, result) {
    if (err) return callback(err);
    if (!result || result.length === 0) return callback(null, thing, 0);
    callback(null, thing, result[0].value);
  });
}

function getDayDifferenceConsumoPermeado(thing, yesterdayConsumoPermeado, callback) {
  if (yesterdayConsumoPermeado == 0) return callback(null, thing, 0);
  var yesterdayEndEnd = moment().endOf('day').add(-2, 'days');
  var params = {endDate: yesterdayEndEnd.toJSON(), lib: 'panel'};
  thethingsAPI.thingRead(thing.thingToken, 'Consumo_Permeado', params, function(err, result) {
    if (err) return callback(err);
    if (!result || result.length === 0) return callback(null, thing, 0);
    var diferencia = (yesterdayConsumoPermeado - result[0].value);
    if (diferencia < 0) diferencia = yesterdayConsumoPermeado;
    callback(null, thing, diferencia.toFixed(2));
  });
}

function saveConsumoDiario(thing, dayDifference, callback) {
  var yesterdayEnd = moment().endOf('day').add(-1, 'days').add(-12, 'hours');
  var values = {values:[{key: 'consumoPorDia', value: dayDifference, datetime: yesterdayEnd.toJSON()}]};
  thethingsAPI.thingWrite(thing.thingToken, values, {lib:'panel'}, callback);
}
