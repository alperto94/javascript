var monthsJSON = {
  "January": {
    "en": "January",
    "es": "Enero",
    "de": "Januar",
    "fr": "Janvier",
    "it": "Gennaio",
    "ca": "Gener",
    "pt": "Janeiro"
  },
  "February": {
    "en": "February",
    "es": "Febrero",
    "de": "Februar",
    "fr": "Février",
    "it": "Febbraio",
    "ca": "Febrer",
    "pt": "Fevereiro"
  },
  "March": {
    "en": "March",
    "es": "Marzo",
    "de": "März",
    "fr": "Mars",
    "it": "Marzo",
    "ca": "Març",
    "pt": "Março"
  },
  "April": {
    "en": "April",
    "es": "Abril",
    "de": "April",
    "fr": "Avril",
    "it": "Aprile",
    "ca": "Abril",
    "pt": "Abril"
  },
  "May": {
    "en": "May",
    "es": "Mayo",
    "de": "Mai",
    "fr": "Mai",
    "it": "Maggio",
    "ca": "Maig",
    "pt": "Maio"
  },
  "June": {
    "en": "June",
    "es": "Junio",
    "de": "Juni",
    "fr": "Juin",
    "it": "Giugno",
    "ca": "Juny",
    "pt": "Junho"
  },
  "July": {
    "en": "July",
    "es": "Julio",
    "de": "Juli",
    "fr": "Juin",
    "it": "Luglio",
    "ca": "Juliol",
    "pt": "Julho"
  },
  "August": {
    "en": "August",
    "es": "Agosto",
    "de": "August",
    "fr": "Août",
    "it": "Agosto",
    "ca": "Agost",
    "pt": "Agosto"
  },
  "September": {
    "en": "September",
    "es": "Septiembre",
    "de": "September",
    "fr": "Septembre",
    "it": "Settembre",
    "ca": "Setembre",
    "pt": "Setembro"
  },
  "October": {
    "en": "October",
    "es": "Octubre",
    "de": "Oktober",
    "fr": "Octobre",
    "it": "Ottobre",
    "ca": "Octubre",
    "pt": "Outubro"
  },
  "November": {
    "en": "November",
    "es": "Noviembre",
    "de": "November",
    "fr": "Novembre",
    "it": "Novembre",
    "ca": "Novembre",
    "pt": "Novembro"
  },
  "December": {
    "en": "December",
    "es": "Diciembre",
    "de": "Dezember",
    "fr": "Décembre",
    "it": "Dicembre",
    "ca": "Desembre",
    "pt": "Dezembro"
  }
};

var translations = {
  "sedimentos" : {
    "en": "Sediments",
    "es": "Sedimentos",
    "de": "Sedimente",
    "fr": "Sédiments",
    "it": "Sedimenti",
    "ca": "Sediments",
    "pt": "Sedimentos"

  },
  "carbon" : {
    "en": "Carbon",
    "es": "Carbón",
    "de": "Kohlenstoff",
    "fr": "Charbon",
    "it": "Carbone",
    "ca": "Carbó",
    "pt": "Carvão"
  },
  "membrana" : {
    "en": "Membrane",
    "es": "Membrana",
    "de": "Membran",
    "fr": "Membrane",
    "it": "Membrana",
    "ca": "Membrana",
    "pt": "Membrana"
  },
  "postfiltro" : {
    "en": "Post-filter",
    "es": "Postfiltro",
    "de": "Nachfilter",
    "fr": "Post-filtre",
    "it": "Postfiltro",
    "ca": "Postfiltres",
    "pt": "Pós-filtro"
  },
  "litros" : {
    "en": "litres",
    "es": "litros",
    "de": "Liter",
    "fr": "litres",
    "it": "litri",
    "ca": "litres",
    "pt": "litros"
  },
  "ahorro" : {
    "en": "SAVINGS",
    "es": "HAS AHORRADO",
    "de": "EINSPARUNG",
    "fr": "ÉCONOMIE",
    "it": "RISPARMIO",
    "ca": "HAS ESTALVIAT",
    "pt": "POUPANÇA"
  },
  "envases" : {
    "en": "plastic bottles (1,5 l)",
    "es": "envases de plástico 1,5 l",
    "de": "Behälter (1,5 Liter)",
    "fr": "emballages (1,5 l)",
    "it": "bottiglie (1,5 l)",
    "ca": "envasos de plàstic 1,5 l",
    "pt": "recipientes (1,5 l)"
  },
  "recuperacion" : {
    "en": "The water recovered is the water that the equipment reincorporates in the circuit after the osmosis cycle. This means that you can cut back on your consumption.",
    "es": "El agua recuperada es el agua que el equipo reincorpora al circuito después del ciclo de osmosis. De esta forma puedes ahorrar en tu consumo.",
    "de": "Das rückgewonnene Wasser ist das Wasser, das das Gerät nach dem Osmose Zyklus wieder in den Kreislauf einsetzt. Auf diese Weise können Sie Ihren Verbrauch reduzieren.",
    "fr": "L'eau récupérée est l'eau que l'appareil réincorpore au circuit après le cycle d'osmosis. De cette façon, vous pouvez économiser sur votre consommation.",
    "it": "L'acqua recuperata è l'acqua che l'apparecchiatura reintegra nel circuito dopo il ciclo di osmosi. In questo modo è possibile risparmiare sui consumi.",
    "ca": "L'aigua recuperada és l'aigua que l'equip reincorpora al circuit després del cicle de l'òsmosis. D'aquesta forma pots estalviar en el teu consum.",
    "pt": "A água recuperada é a água que o equipamento reincorpora no circuito depois do ciclo de osmose. Desta forma pode poupar no seu consumo."
  },
    "email" : {
    "en": "sat@corsa.es",
    "es": "sat@corsa.es",
    "de": "sat@corsa.es",
    "fr": "sat@corsa.es",
    "it": "osmoticzero@iwmceasa.it",
    "ca": "sat@corsa.es",
    "pt": "sat@corsa.es"
  },
    "telefon" : {
    "en": "902 235 375",
    "es": "902 235 375",
    "de": "902 235 375",
    "fr": "902 235 375",
    "it": "+39 3386740855",
    "ca": "902 235 375",
    "pt": "902 235 375"
  },
    "whatsapp" : {
    "en": "+34695 236 937",
    "es": "695 236 937",
    "de": "+34695 236 937",
    "fr": "+34695 236 937",
    "it": "",
    "ca": "695 236 937",
    "pt": "+34695 236 937"
  },
    "text_whatsapp" : {
    "en": "Hello%20i%20am%20Osmotic%20client%20and%20",
    "es": "Hola%20soy%20cliente%20de%20Osmotic%20Zero%20y%20",
    "de": "Hola%20soy%20cliente%20de%20Osmotic%20Zero%20y%20",
    "fr": "Hola%20soy%20cliente%20de%20Osmotic%20Zero%20y%20",
    "it": "Ciao%20sono%20Osmotic%20Zero%20utente%20",
    "ca": "Hola%20soc%20client%20d'Osmotic%20Zero%20i%20",
    "pt": "Hola%20soy%20cliente%20de%20Osmotic%20Zero%20y%20"
  }
}

var images = {
  "estadoTodoOK" : {
    "en": "https://blog.thethings.io/wp-content/uploads/2018/10/estados2-EN.png",
    "es": "https://blog.thethings.io/wp-content/uploads/2018/04/estados2.png",
    "de": "https://blog.thethings.io/wp-content/uploads/2018/10/estados2-DE.png",
    "fr": "https://blog.thethings.io/wp-content/uploads/2018/10/estados2-FR.png",
    "it": "https://blog.thethings.io/wp-content/uploads/2018/10/estados2-IT.png",
    "ca": "https://blog.thethings.io/wp-content/uploads/2018/09/estados2-cat.png",
    "pt": "https://blog.thethings.io/wp-content/uploads/2018/10/estados2-PT.png"
  },
  "estadoErrorAguaFiltroOK" : {
    "en": "https://blog.thethings.io/wp-content/uploads/2018/10/error-filtrosOK-EN.png",
    "es": "https://blog.thethings.io/wp-content/uploads/2018/06/error-filtrosOK.png",
    "de": "https://blog.thethings.io/wp-content/uploads/2018/10/error-filtrosOK-DE.png",
    "fr": "https://blog.thethings.io/wp-content/uploads/2018/10/error-filtrosOK-FR.png",
    "it": "https://blog.thethings.io/wp-content/uploads/2018/10/error-filtrosOK-IT.png",
    "ca": "https://blog.thethings.io/wp-content/uploads/2018/09/error-filtrosOK-cat.png",
    "pt": "https://blog.thethings.io/wp-content/uploads/2018/10/error-filtrosOK-PT.png"
  },
  "estadoAguaOKFiltrosError" : {
    "en": "https://blog.thethings.io/wp-content/uploads/2018/10/aguaoptima-error-EN.png",
    "es": "https://blog.thethings.io/wp-content/uploads/2018/06/aguaoptima-error.png",
    "de": "https://blog.thethings.io/wp-content/uploads/2018/10/aguaoptima-error-DE.png",
    "fr": "https://blog.thethings.io/wp-content/uploads/2018/10/aguaoptima-error-FR.png",
    "it": "https://blog.thethings.io/wp-content/uploads/2018/10/aguaoptima-error-IT.png",
    "ca": "https://blog.thethings.io/wp-content/uploads/2018/09/aguaoptima-error-cat.png",
    "pt": "https://blog.thethings.io/wp-content/uploads/2018/10/aguaoptima-error-PT.png"
  }
}

function job(params, callback){
  var start = Date.now();
  var thingsWithError = [];
  thethingsAPI.getProductThings(function(err, things) {
    if (err) return callback(err);
    var productionThings = things.filter(thing => {
        var productTag = thing.tags.filter(tag => (tag._id == 'prod'));
        return productTag.length > 0;
    });

    if (productionThings == undefined || productionThings.length == 0) return callback(null, "finished");

    async.eachLimit(productionThings, 10, function(thing, next) {
      if(thing.hasOwnProperty('description'))
          {
           	if(thing.description.hasOwnProperty('name'))
            	console.log(thing.description.name)
         	else
         		console.log('without name')
          }
          else
            console.log('without name')
      async.waterfall([
        async.apply(getConsumoPermeadoCurrentMonth, thing),
        getRecuperacionCurrentMonth,
        getCNivelesFiltros,
        getTDS,
        getTDSLimit,
        getTestMode,
        getEstadoComponente,
        getMonth,
        produceHTML,
        saveHTML
      ], function(err, result) {
        if(err) thingsWithError.push(thing)
        next();
      });
    }, function(err){
      if(err) return callback(err)

      /*
      // El problema es que se llena de mierda porque hay unas 200 things de prueba que fallan.
      // Revisar este codigo.
      if(thingsWithError.length != 0){
        var res = 'Job done. ' + things.length + ' were processed. '+ thingsWithError.length + ' have an error. Execution time: ' + parseInt((Date.now()-start)/1000) + ' seconds';
        var errorKpi = thingsWithError.map(function(thing) {
          if(thing.hasOwnProperty('description'))
          {
           	if(thing.description.hasOwnProperty('name'))
            	return thing.description.name
         	else
         		return thing.thingToken;
          }
          else
            return thing.thingToken;
        })
        analytics.kpis.create('thingsWithErrorHourlyJob', errorKpi)
      }
      else{
        var res = 'Job done. ' + things.length + ' were processed. Execution time: ' + parseInt((Date.now()-start)/1000) + ' seconds';
      }
      */
      callback(null, res);
    });
  });
}

function getConsumoPermeadoCurrentMonth(thing, callback) {
  var monthStart = moment().startOf('month');
  var params = {endDate: monthStart.toISOString(), lib: 'panel'};
  thethingsAPI.thingRead(thing.thingToken, 'Consumo_Permeado', params, function(err1, result1) {
    thethingsAPI.thingRead(thing.thingToken, 'Consumo_Permeado', {lib:'panel'}, function(err2, result2) {
      var firstValue = (!result1 || result1.length === 0) ? 0 : result1[0].value;
      var lastValue = (!result2 || result2.length === 0) ? 0 : result2[0].value;
      var consumoPermeadoCurrentMonth = lastValue-firstValue;
      callback(null, thing, consumoPermeadoCurrentMonth);
    });
  });
}

function getRecuperacionCurrentMonth(thing, consumoPermeadoCurrentMonth, callback) {
  var monthStart = moment().startOf('month');
  var params = {endDate: monthStart.toISOString(), lib: 'panel'};
  thethingsAPI.thingRead(thing.thingToken, 'Recuperacion', params, function(err1, result1) {
    thethingsAPI.thingRead(thing.thingToken, 'Recuperacion', {lib:'panel'}, function(err2, result2) {
      var firstValue = (!result1 || result1.length === 0) ? 0 : result1[0].value;
      var lastValue = (!result2 || result2.length === 0) ? 0 : result2[0].value
      var recuperacionCurrentMonth = lastValue-firstValue;
      callback(null, thing, consumoPermeadoCurrentMonth, recuperacionCurrentMonth);
    });
  });
}

function getCNivelesFiltros(thing, consumoPermeadoCurrentMonth, recuperacionCurrentMonth, callback) {
  thethingsAPI.thingRead(thing.thingToken, 'Niveles_Filtros', {lib:'panel'}, function(err, result) {
    if (!result || result.length === 0) return callback("error");
    var cNivelesFiltros = result[0].value;
    callback(null, thing, consumoPermeadoCurrentMonth, recuperacionCurrentMonth, cNivelesFiltros);
  });
}

function getTDS(thing, consumoPermeadoCurrentMonth, recuperacionCurrentMonth, cNivelesFiltros, callback) {
  thethingsAPI.thingRead(thing.thingToken, 'TDS', {lib:'panel'}, function(err, result) {
    if (!result || result.length === 0) return callback(null, thing, consumoPermeadoCurrentMonth, recuperacionCurrentMonth, cNivelesFiltros, -1)
    var tds = result[0].value;
    callback(null, thing, consumoPermeadoCurrentMonth, recuperacionCurrentMonth, cNivelesFiltros, tds);
  });
}

function getTDSLimit(thing, consumoPermeadoCurrentMonth, recuperacionCurrentMonth, cNivelesFiltros, tds, callback) {
  thethingsAPI.thingRead(thing.thingToken, 'tds_limit', {lib:'panel'}, function(err, result) {
    if (!result || result.length === 0) return callback(null, thing, consumoPermeadoCurrentMonth, recuperacionCurrentMonth, cNivelesFiltros, tds, -1);
    var tds_limit = result[0].value;
    callback(null, thing, consumoPermeadoCurrentMonth, recuperacionCurrentMonth, cNivelesFiltros, tds, tds_limit);
  });
}

function getTestMode(thing, consumoPermeadoCurrentMonth, recuperacionCurrentMonth, cNivelesFiltros, tds, tds_limit, callback){
  thethingsAPI.thingRead(thing.thingToken, 'test_mode', {lib:'panel'}, function(err, result) {
    if (!result || result.length === 0) var testmode = false;
    else if ((result[0].value == 1) && (moment().diff(moment(result[0].datetime), 'minutes') > 60)) var testmode = true;
    else var testmode = false;
    callback(null, thing, consumoPermeadoCurrentMonth, recuperacionCurrentMonth, cNivelesFiltros, tds, tds_limit, testmode);
  });
}

function getEstadoComponente(thing, consumoPermeadoCurrentMonth, recuperacionCurrentMonth, cNivelesFiltros, tds, tds_limit, testmode, callback){
  thethingsAPI.thingRead(thing.thingToken, 'Estado_Componente', {lib:'panel'}, function(err, result) {
    if (!result || result.length === 0) var estadoComponente = 0;
    else var estadoComponente = result[0].value;
    callback(null, thing, consumoPermeadoCurrentMonth, recuperacionCurrentMonth, cNivelesFiltros, tds, tds_limit, testmode, estadoComponente);
  });
}


function getMonth(thing, consumoPermeadoCurrentMonth, recuperacionCurrentMonth, cNivelesFiltros, tds, tds_limit, testmode, estadoComponente, callback){
    if (!thing.description.lan || thing.description.lan == null) var language = 'es';
    else var language = thing.description.lan;
  	//language = 'es'; // REMOVE WHEN TRANSLATIONS ARE READY
    callback(null, thing, consumoPermeadoCurrentMonth, recuperacionCurrentMonth, cNivelesFiltros, tds, tds_limit, testmode, estadoComponente,  language);
}

function produceHTML(thing, cConsumo_Permeado, cRecuperacion, cNivelesFiltros, tds, tds_limit, testmode, estadoComponente, language, callback) {
  var values = [];
  var month = monthsJSON[moment().format('MMMM')][language];;

  const estadoTodoOK = images["estadoTodoOK"][language];
  const estadoErrorAguaFiltroOK = images["estadoErrorAguaFiltroOK"][language];
  const estadoAguaOKFiltrosError = images["estadoAguaOKFiltrosError"][language];

  var estado;

  if((tds <= tds_limit) || (tds == -1 || tds_limit == -1) || (estadoComponente == "0000000000000000")) // okey
    estado = estadoTodoOK;
  else if(estadoComponente !== "0000000000000000")
    estado = estadoAguaOKFiltrosError;
  else // error
    estado = estadoErrorAguaFiltroOK;

  let estadosHTML = "<div class='corsa-water-quality-container corsa-mobile-friendly'>"
      + "<div class='corsa-water-quality-text' style='text-align:center;'>"
      + "<img src='"+estado+"' class='corsa-img-responsive' />"
      + "</div>"
      + "</div>";

  values.push({
    "key": "$settings.estados",
    "value": estadosHTML
  });

  let nivelFiltrosHTML = "<div class='corsa-filters-container corsa-mobile-friendly'>"
    + "<div class='corsa-filters-icons'>"
    + "<div class='corsa-filters-icon'>"
    + "<img src='https://blog.thethings.io/wp-content/uploads/2018/04/filtros" + cNivelesFiltros.charAt(0) + ".png' class='corsa-img-responsive'>"
    + "<br />"
    + "<div class='corsa-titulos-filtros'>" + translations["sedimentos"][language] + "</div>"
    + "</div>"
    + "<div class='corsa-filters-icon'>"
    + "<img src='https://blog.thethings.io/wp-content/uploads/2018/04/filtros" + cNivelesFiltros.charAt(1) + ".png' class='corsa-img-responsive'>"
    + "<br />"
    + "<div class='corsa-titulos-filtros'>" + translations["carbon"][language] + "</div>"
    + "</div>"
    + "<div class='corsa-filters-icon'>"
    + "<img src='https://blog.thethings.io/wp-content/uploads/2018/04/filtros" + cNivelesFiltros.charAt(2) + ".png' class='corsa-img-responsive'>"
    + "<br />"
    + "<div class='corsa-titulos-filtros'>" + translations["membrana"][language] + "</div>"
    + "</div>"
    + "<div class='corsa-filters-icon'>"
    + "<img src='https://blog.thethings.io/wp-content/uploads/2018/04/filtros" + cNivelesFiltros.charAt(3) + ".png' class='corsa-img-responsive'>"
    + "<br />"
    + "<div class='corsa-titulos-filtros'>" + translations["postfiltro"][language] + "</div>"
    + "</div>"
    + "</div>"
    + "</div>";

  values.push({
    "key": "$settings.nivelFiltros",
    "value": nivelFiltrosHTML
  });

  let fecha = month + ' ' + moment().format('YYYY');
  let ahorro = Math.ceil(cConsumo_Permeado / 1.5);
  ahorro = Math.round(ahorro * 100) / 100;

  cConsumo_Permeado = Math.round(cConsumo_Permeado * 100) / 100;

  let consumoHTML = "<div class='corsa-tree-container mobile-friendly'>"
    + "<div class='corsa-tree-row'>"
    + "<div class='corsa-tree-text-container'>"
    + "<div class='corsa-tree-text'>"
    + "<span class='corsa-liters'>"+ cConsumo_Permeado + " " + translations["litros"][language] + "</span>"
    + "<br />"
    + "<span class='corsa-date'>" + fecha +"</span>"
    + "</div>"
    + "<br><br><br>"
    + "<div>"
    + "<img src='https://blog.thethings.io/wp-content/uploads/2018/04/recycle.png' class='img-recycle'>"
    + "<b>" + translations["ahorro"][language] + "</b><br>"+ ahorro + " " + translations["envases"][language]
    + "</div>"
    + "</div>"
    + "<div class='corsa-tree-image-container-bottles'>"
    + "<img src='https://blog.thethings.io/wp-content/uploads/2018/04/botellas.png' class='img-responsive'>"
    + "</div>"
    + "</div>"
    + "</div>";

  values.push({
    "key": "$settings.consumoHTML",
    "value": consumoHTML
  });


  let contactHTML = "<div class='corsa-filters-container corsa-mobile-friendly'>"
    +"<div class='corsa-filters-icons'>"
    +"    <div class='corsa-contact-icon'>"
    +"      <center>"
    +"        <a href='tel:"+ translations["telefon"][language] +"'>"
    +"          <img src='https://blog.thethings.io/wp-content/uploads/2018/04/telefono.png' class='img-responsive-contact' />"
    +"          <div class='corsa-titulos-filtros'>"+ translations["telefon"][language] +"</div>"
    +"        </a>"
    +"      </center>"
    +"    </div>"
    +"    <div class='corsa-contact-icon'>"
    +"      <center>"
    +"        <a href='mailto:"+ translations["email"][language] +"'>"
    +"          <img src='https://blog.thethings.io/wp-content/uploads/2018/04/email.png' class='img-responsive-contact' />"
    +"          <div class='corsa-titulos-filtros'>"+ translations["email"][language] +"</div>"
    +"        </a>"
    +"      </center>"
    +"    </div>";

  if (translations["whatsapp"][language] != "")
  {
    contactHTML += "    <div class='corsa-contact-icon'>"
                    +"      <center>"
                    +"        <a href='https://api.whatsapp.com/send?phone="+ translations["whatsapp"][language] +"&text="+ translations["text_whatsapp"][language] +"'>"
                    +"          <img src='https://blog.thethings.io/wp-content/uploads/2018/04/whatsapp.png' class='img-responsive-contact' />"
                    +"          <div class='corsa-titulos-filtros'>"+ translations["whatsapp"][language] +"</div>"
                    +"        </a>"
                    +"      </center>"
                    +"    </div>";
   }


	contactHTML += "	</div></div>";

    values.push({
      "key": "$settings.contactHTML",
      "value": contactHTML
    });


  cRecuperacion = Math.round(cRecuperacion * 100) / 100;

  let recuperacionHTML = "<div class='corsa-tree-container mobile-friendly'>"
    + "<div class='corsa-tree-row'>"
    + "<div class='corsa-tree-text-container'>"
    + "<div class='corsa-tree-text'>"
    + "<p>" + translations["recuperacion"][language]
    + "<br>"
    + "<div class='corsa-tree-text-recuperada'>"
    + "<span class='corsa-tree-text-litros-recuperados'>"+ cRecuperacion + "</span> " + translations["litros"][language]
    + "</div>"
    + "<br>"
    + "<center>" + fecha + "</center>"
    + "</p>"
    + "</div>"
    + "</div>"
    + "</div>"
    + "<div class='corsa-tree-row'>"
    + "<div class='corsa-tree-text-container'>"
    + "<div class='corsa-tree-text'>"
    + "<img class='corsa-tree-image' src='https://blog.thethings.io/wp-content/uploads/2018/03/corsa-tree-50.png' class='img-responsive'>"
    + "</div>"
    + "</div>"
    + "</div>"
    + "</div>";

  values.push({
    "key": "$settings.recuperacionHTML",
    "value": recuperacionHTML
  });
  return callback(null, thing, values, testmode)
}

function saveHTML(thing, data, testmode, callback) {
  var result = data;
  if(testmode)
  {
    result.push({
      "key": "log_errores",
      "value": "Test mode lleva activo más de una hora"
    })
  }
  thethingsAPI.thingWrite(thing.thingToken, {values : result}, {lib:'panel'}, function(err,result){
    if (err) return callback(err);
    else return callback();
  });
}
