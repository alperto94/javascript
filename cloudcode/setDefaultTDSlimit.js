// Organisasion: CORSA; Product: Osmotic Zero
// Script para que en todos los things con el tag PROD se ponga el recurso tds_limit a 300

function main(params, callback){
   let tds_limit = [{"key": "tds_limit", "value": 300}];
   thethingsAPI.getProductThings(function(err, things) {
     async.each(things, function(t, next) {
        let filtered = t.tags.filter(tag => tag.name === 'PROD');
        if (filtered.length <= 0) return next();
        thethingsAPI.thingWrite(t.thingToken, {"values": tds_limit}, {lib: 'panel'}, function(err,result){
          if (err) return next(err);
          else return next();
        });
      },

      function(err) {
        callback(null,"Executed fine."");
      });
    });
}
