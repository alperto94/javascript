// Organisasion: CORSA; Product: Osmotic Zero
// Script para contar todos los things del producto y los things con el tag PROD

function main(params, callback){
   let cont_total = 0;
   let cont_parcial = 0;
   let tds_limit = [{"key": "tds_limit", "value": 300}];
   thethingsAPI.getProductThings(function(err, things) {
     async.each(things, function(t, next) {
        ++cont_total;
        let filtered = t.tags.filter(tag => tag.name === 'PROD');
        if (filtered.length > 0) {++cont_parcial;}
        return next();
        /*thethingsAPI.thingWrite(t.thingToken, {"values": tds_limit}, {lib: 'panel'}, function(err,result){
        if (err) return next(err);
        else return next();
  });*/
      },

      function(err) {

      callback(null,"Executed fine. " + cont_parcial + " / " + cont_total);
    });
  });
}
