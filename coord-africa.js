const cuadrado = [{"lat":26,"lng":-25},{"lat":-20,"lng":-25},{"lat":-20,"lng":30},{"lat":26,"lng":30}];

function max (a, b) {
  if (a > b) return a;
  else return b;
}

function min (a,b) {
  if (a > b) return b;
  else return a;
}

function insideSquare(point) {
  var up, down, left, right;
  up = max(cuadrado[0].lng, cuadrado[1].lng);
  up = max(up, cuadrado[2].lng);
  down = min(cuadrado[0].lng, cuadrado[1].lng);
  down = min(down, cuadrado[2].lng);
  right = max(cuadrado[0].lat, cuadrado[1].lat);
  right = max(right, cuadrado[2].lat);
  left = min(cuadrado[0].lat, cuadrado[1].lat);
  left = min(left, cuadrado[2].lat);
  return (point.lat > left && point.lat < right && point.lng > down && point.lng < up);
}

if (insideSquare({"lat":25.517770620141576,"lng":-13.924389883394497})) console.log("yes");
else console.log("no");

/*
a saco: ret = (point.lat > -20 && point.lat < 34 && point.lng > -25 && point.lng < 30);
{"lat":22.44980738520949, "lng":1.7768606543540955} (yes)
{"lat":29.448777056905413,"lng":-87.66128586012121} (no)
{"lat":28.106407925068197,"lng":-16.1236948790189} (canarias -> no)
{"lat":28.239440112345765,"lng":-13.829777423709174} (canarias2 -> no)
{"lat":25.517770620141576,"lng":-13.924389883394497} (yes)
*/
