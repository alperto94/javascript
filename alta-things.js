var theThingsAPI = require('./apiclient_js');
var fs = require('fs');
var client = new theThingsAPI.ApiClient({host: 'https://api.thethings.io'});
var async = require('async');
var request = require("request");

var file = fs.readFileSync('alta-things-data/customer-address-list-sin-cabecera.csv', 'utf8');
var splited = file.trim().split('\n');

function writeThing(token, element) {
  var options_write = { method: 'POST',
    url: 'https://api.thethings.io/v2/things/' + token,
    body: { values:
      [ { key: '$geo', value: [element[9],element[8]] },
        { key: '$settings.name', value: element[0] },
        { key: '$settings.alias', value: element[1] },
        { key: '$settings.partyClassificationGroup', value: element[2] },
        { key: '$settings.customerClassificationGroup', value: element[3] } ] },
    json: true };

  request(options_write, function (error, response, body) {
    if (error) throw new Error(error);
    console.log("[writeThing] Written with success thing" , token);
  });
}

function writeTags(token, element) {
  var options_tags = { method: 'POST',
    url: 'https://api.thethings.io/v2/things/' + token + '/tags',
    body: { "name":[element[2], element[3], element[4], element[5], element[6]] },
    json: true };

  request(options_tags, function (error, response, body) {
    if (error) throw new Error(error);
    console.log("[writeTag] Written with success tags in thing", token);
  });
}
// console.log(file);

var data = [];
splited.forEach(function(value, index, array) {
  var v = [];
  v = value.split(',');
  v[8] = v[8].replace('"','');
  v[9] = v[9].replace('"','');
  v[8] = v[8].replace('\r','');
  v[9] = v[9].replace('\r','');
  data.push(v);
});

async.forEach(data, function (value, callback) {
  var options = { method: 'POST',
    url: 'https://api.thethings.io/v2/things',
    body: { activationCode: 'Y5ChNDN7K1A2Z8e5xdF53nGtQfFnafGdYg', productId: '19895' },
    json: true };

  request(options, function (error, response, body) {
    // if (!error && response.statusCode == 200) {}
    if (error) throw new Error(error);
    console.log("Thing activated with token", body.thingToken);
    writeThing(body.thingToken, value);
    writeTags(body.thingToken, value);
    return callback();
  });
}, function (err) {
    if (err) console.error(err.message);
    console.log("Execution ended");
});


// Para cada linea del csv, cojo el objeto y: (en un async pq hay requests!)
// 1) Activo un thing (mismo codigo todo el rato) y me quedo con el thingToken que devuelve la llamadas
// doneeeeee
// /* 2) Hago un thing write usando el token */
//
//
// var options_write = { method: 'POST',
//   url: 'https://api.thethings.io/v2/things/THINGTOKEN',
//   body: { values: [ { key: 'gtgt', value: '{frr}' } ] },
//   json: true };
//
// request(options, function (error, response, body) {
//   if (error) throw new Error(error);
//
//   console.log(body);
// });


//  Respuesta del Activate
// {
//   "status": "created",
//   "message": "thing activated",
//   "thingToken": "14z5J3LDC0xyAGWCV7VX_B0_AgX2rZ8QYZzsunWGIu4",
//   "thingId": "t9rSS3of1Yk-xM9uNZHV7ZXd8CnokHhWptS-3lGCHUQ"
// }
