var request = require("request");
var fs = require('fs');
var async = require("async");

var username = "5bd2f159e833d93f4049db48",
    password = "9cbc737bcfffdc330e85ba663ffdd4b9",
    auth = "Basic " + new Buffer(username + ":" + password).toString("base64");

var file = fs.readFileSync('redundancy-sigfox-data/sigfox_lat_and_lon.csv', 'utf8');
var splited = file.trim().split('\n');

var result = [];
var control = 0;
async.forEachOf(splited, function(value, index, callback) {
  var v = value.split(',');
  var options = { method: 'GET',
    url: 'https://backend.sigfox.com/api/coverages/redundancy',
    qs: { lat: v[0], lng: v[1] },
    headers:
     { "Authorization" : auth,
       'cache-control': 'no-cache' } };

  request(options, function (error, response, body) {
   if (error) throw new Error(error);
   result[index] = JSON.parse(body);
   if (++control % 50 === 0) console.log(control + " processed...");
   callback();
  });

}, function (err) {
    if (err) console.error(err.message);
    // configs is now a map of JSON data
    // for (var a in result) console.log(a + ": " + result[a].redundancy);
    var fileResult = fs.createWriteStream('redundancy-sigfox-data/result2.txt');
    fileResult.on('error', function(err) { /* error handling */ });
    result.forEach(function(v) { fileResult.write(v.redundancy + '\n'); });
    fileResult.end();
    console.log("Execution ended");
});
